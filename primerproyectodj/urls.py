from django.conf.urls import patterns,include, url #include para la pagina admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^preguntas/$','preguntasyrespuestas.views.index', name='preguntas'),
	url(r'^preguntas/(?P<pregunta_id>\d+)/$','preguntasyrespuestas.views.pregunta_detalle',name='pregunta_detalle'),#cuidar que P es mayuscula
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),#esta url para el admin
    url(r'^admin/',include(admin.site.urls)),#solo es un extra
    url(r'^preguntas/crear/$','preguntasyrespuestas.views.pregunta_crear',name='pregunta_crear'),
    url(r'^preguntas/editar/(?P<pregunta_id>\d+)/$','preguntasyrespuestas.views.pregunta_editar', name='pregunta_editar'),
    url(r'^login/$','primerproyectodj.views.login_page',name="login"),
    url(r'^$','primerproyectodj.views.homepage',name="homepage"),
    url(r'^logout/$','primerproyectodj.views.logout_view',name="logout"),

















)