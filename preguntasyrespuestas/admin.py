from django.contrib import admin
from preguntasyrespuestas.models import Pregunta,Respuesta

class RespuestaInLine(admin.StackedInline):
    model = Respuesta
    extra = 3

class PreguntaAdmin(admin.ModelAdmin):
    inlines = [RespuestaInLine]
    list_display = ('asunto','fecha_publicacion','publicado_hoy')#funcionalidad en admin, muestra las columnas del modelo
    list_filter = ['fecha_publicacion']#otra funcionalidad, a la derecha aparece el panel para organizar las publicaciones



admin.site.register(Pregunta, PreguntaAdmin)
admin.site.register(Respuesta)


























